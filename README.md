# README

## Info
This is a (WIP) todo-list app using the new React Hooks:

*Frontend:*
* React
* React Router
* React Hooks
* Axios

*Backend:*
* Ruby on Rails
* PostgreSQL


## Usage 

**Run the server:**

`$ cd todo-list-server`

`$ rails s -p 4000`

**Run the client:**

`$ cd todo-list-client`

`$ yarn install`

`$ yarn start`

_You need to run server and client at the same time._
