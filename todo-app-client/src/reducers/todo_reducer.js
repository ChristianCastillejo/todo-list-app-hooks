import { FETCH_TODO_LIST } from "../actions/action_types";

// For safety: always define an initial state. Notice that the initialState when create an store will override this value.
const init = {
  todoList: []
};

export function todoListReducer(state = init, action) {
  switch (action.type) {
    case FETCH_TODO_LIST: {
      return action.payload.data;
    }
    default: {
      return state;
    }
  }
}
