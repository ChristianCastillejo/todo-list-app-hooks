import { todoListReducer } from "./todo_reducer";

// Redux look-a-like combineReducers functions
const combineReducers = reducer => (state = {}, action) => {
  const keys = Object.keys(reducer);
  const nextReducers = {};
  for (let i = 0; i < keys.length; i += 1) {
    nextReducers[keys[i]] = reducer[keys[i]](state[keys[i]], action);
  }
  return nextReducers;
};

const rootReducer = combineReducers({
  todoList: todoListReducer
});

export default rootReducer;
