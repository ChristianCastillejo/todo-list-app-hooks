import React from "react";
import PropTypes from "prop-types";

export const Store = React.createContext();

// Mainly thanks to:
// / https://dev.to/hirodeath/similar-redux-architecture-example-powered-by-react-hooks-api-hdg
// and
// that link I send to chris

/**
 // Create store class like redux, which you can call getState() but not change anything as it's an anti-pattern
 * @param {object} state State
 * @param {function} dispatch Dispatch
 * @return {object} Store
 */
const createStoreVariable = ({ state, dispatch }) => ({
  getState: () => state,
  dispatch
});

// https://medium.com/maxime-heckel/rebuilding-redux-with-hooks-and-context-e16b59faf51c
const compose = (...funcs) => x =>
  funcs.reduceRight((composed, f) => f(composed), x);

export const createStore = (reducer, initialState, middlewares) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  if (typeof middlewares !== "undefined") {
    const promises = React.useRef([]);
    const middlewareAPI = Object.assign(
      createStoreVariable({ state, dispatch }),
      {
        callbackWhenFinished: middlewarePromise =>
          promises.current.push(middlewarePromise)
      }
    );
    // Callback all middleware to inform that there is a new state, when middleware registered themselves.
    React.useEffect(
      () => {
        let didCancel = false;
        promises.current.forEach(middlewarePromise =>
          !didCancel
            ? middlewarePromise.resolve(middlewareAPI)
            : middlewarePromise.reject(middlewareAPI)
        );
        promises.current = []; // Cleanup element to remove (unhandled) promises.
        return () => {
          didCancel = true; // Element is being unmounted, reject all pending promises.
        };
      },
      [middlewareAPI, state]
    );
    const chain = middlewares.map(middleware => middleware(middlewareAPI));
    const enhancedDispatch = compose(...chain)(dispatch);

    return { state, dispatch: enhancedDispatch };
  }
  return { state, dispatch };
};

/**
 * React context provider export function that creates a global store (like redux)
 * @param {object} children React components
 * @param {object} reducer Reducer or rootReducer
 * @param {object} initialState Object containing initial state.
 * @param {array} middleware Array container middleware functions
 * @return {Object} React store provider
 */
export function StoreProvider({ children, reducer, initialState, middleware }) {
  const store = createStore(reducer, initialState, middleware);
  return <Store.Provider value={store}>{children}</Store.Provider>;
}
StoreProvider.propTypes = {
  children: PropTypes.element.isRequired,
  reducer: PropTypes.func.isRequired,
  initialState: PropTypes.objectOf(PropTypes.any),
  middleware: PropTypes.array
};
StoreProvider.defaultProps = {
  initialState: {},
  middleware: []
};

// Redux-look-a-like connect function
export function connect(
  mapStateToProps = () => {},
  mapDispatchToProps = () => {}
) {
  return WrappedComponent => props => {
    const { state, dispatch } = React.useContext(Store);
    const store = createStoreVariable(React.useContext(Store));
    return (
      <WrappedComponent
        dispatch={dispatch}
        {...mapStateToProps({ state, props })}
        {...mapDispatchToProps({ props, store })}
      />
    );
  };
}
