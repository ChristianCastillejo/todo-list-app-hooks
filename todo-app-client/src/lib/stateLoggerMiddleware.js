const stateLoggerMiddleWare = store => next => action => {
  (async () => {
    // Alternative for the redux-logger. Get a deep copy of the old state by JSON copying.
    const stateLogger = {
      past: JSON.parse(JSON.stringify(store.getState())),
      action
    };
    const updatedStore = await new Promise((resolve, reject) =>
      store.callbackWhenFinished({ resolve, reject })
    );
    stateLogger.current = updatedStore.getState();

    console.info("State changed! Info:", stateLogger); //eslint-disable-line
  })();

  // Call the next middleware/action point in the pipeline
  next(action);
};

// Tracking inspiration for future purposes
// const gaMiddleWare = store => next => action => {
//   ( async () => {
//     window.ga && window.ga('send', 'event', 'Action', action.type);
//   })();
//
//   // Call the next middleware/action point in the pipeline
//   next(action);
// };

export default stateLoggerMiddleWare;
