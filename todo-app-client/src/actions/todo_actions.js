import { axiosInstance } from "./configured_axios";
import { FETCH_TODO_LIST } from "./action_types";

const fetchTodoList = async ({ dispatch }) => {
  const request = await axiosInstance.get("/todos/");

  return dispatch({
    type: FETCH_TODO_LIST,
    payload: request
  });
};

const addTodo = async ({ dispatch }, todo) => {
  await axiosInstance.post("/todos/", todo);
  dispatch(fetchTodoList({ dispatch }));
};

const deleteTodo = async ({ dispatch }, todo) => {
  await axiosInstance.delete(`/todos/${todo}`, todo);
  dispatch(fetchTodoList({ dispatch }));
};

export { fetchTodoList, addTodo, deleteTodo };
