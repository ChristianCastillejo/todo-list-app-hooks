export default {
  container: {
    color: "#676767",
    margin: 100,
    backgroundColor: "transparent"
  },
  textArea: {
    padding: 20,
    borderRadius: 10,
    borderColor: "#eeeeee",
    borderWidth: 0,
    backgroundColor: "transparent",
    outline: "none",
    width: "100%",
    height: 120,
    marginBottom: 10
  },
  addTodo: {
    boxShadow: "0px 2px 10px 1px #e1e1e1",
    padding: 10,
    borderRadius: 20,
    color: "#676767",
    background: "linear-gradient(150deg, #fbfbfb, #fcfcfc)",
    height: 200,
    marginRight: 30,
    marginTop: 10
  },
  todoList: { maxHeight: 500, overflow: "auto" },
  todo: {
    boxShadow: "0px 2px 10px 1px #e1e1e1",
    padding: 10,
    paddingLeft: 30,
    borderRadius: 20,
    background: "linear-gradient(150deg, #fbfbfb, #fcfcfc)",
    marginTop: 10,
    marginBottom: 20,
    color: "#676767",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  logo: { borderRadius: "50%" },
  login: {
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
    fontSize: 50,
    color: "#b0b0b0",
    paddingTop: 250,
    fontColor: "#676767"
  },
  listText: {
    textAlign: "center",
    fontSize: 30
  },
  navbar: {
    background: "linear-gradient(150deg, #e1e1e1, #fbfbfb)",
    color: "#676767"
  },
  buttonAdd: { color: "#28CD41" },
  buttonDelete: { color: "#ff375f" }
};
