import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// StoreProvider
import { StoreProvider } from "./lib/store";

// Reducers
import rootReducer from "./reducers/index";

// CSS
import "semantic-ui-css/semantic.min.css";

// Layout
import Layout from "./components/layout";

// Pages
import TodoList from "./components/todo_list";

// Middelware
import stateLoggerMiddleWare from "./lib/stateLoggerMiddleware";

// Set default initial state. Reducers should also initialize a default state, so this doesn't have to be set.
const initialState = {
  todoList: []
};

const App = () => {
  return (
    <StoreProvider
      reducer={rootReducer}
      initialState={initialState}
      middleware={[stateLoggerMiddleWare]}
    >
      <Router>
        <Switch>
          <Layout>
            <Route exact path="/" component={TodoList} />
          </Layout>
        </Switch>
      </Router>
    </StoreProvider>
  );
};

export default App;
