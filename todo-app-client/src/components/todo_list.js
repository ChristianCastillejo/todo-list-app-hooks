import React, { useState } from "react";
import { connect } from "../lib/store";
import { Grid } from "semantic-ui-react";
import { fetchTodoList, addTodo, deleteTodo } from "../actions/index";
import TodoListView from "./todo_list_view";
import AddTodoView from "./add_todo_view";
import styles from "../assets/styles";

function TodoList(props) {
  React.useEffect(() => {
    if (props.todoList.length === 0) {
      props.fetchTodoList();
    }
  }, []);
  const [todo, setTodo] = useState({ title: "" });

  return (
    <div style={styles.container}>
      <Grid>
        <Grid.Row columns={2} style={styles.todoListContainer}>
          <AddTodoView todo={todo} setTodo={setTodo} addTodo={props.addTodo} />
          <TodoListView
            todoList={props.todoList}
            todo={todo}
            deleteTodo={props.deleteTodo}
          />
        </Grid.Row>
      </Grid>
    </div>
  );
}
const mapStateToProps = ({ state }) => ({
  todoList: state.todoList
});

const mapDispatchToProps = ({ store }) => ({
  fetchTodoList: fetchTodoList.bind(null, store),
  addTodo: addTodo.bind(null, store),
  deleteTodo: deleteTodo.bind(null, store)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
