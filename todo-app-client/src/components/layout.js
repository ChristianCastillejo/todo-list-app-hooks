import React from "react";
import { Menu } from "semantic-ui-react";
import styles from "../assets/styles";
import logo from "../assets/logo.png";

export default function Layout(props) {
  const { children } = props;

  return (
    <div>
      <Menu secondary size="large" widths={7} style={styles.navbar}>
        <Menu.Item position="left">
          <img src={logo} style={styles.logo} alt="logo" />
        </Menu.Item>
      </Menu>
      {children}
    </div>
  );
}
