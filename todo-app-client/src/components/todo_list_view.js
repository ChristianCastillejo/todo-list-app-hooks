import React from "react";
import { Grid, Button } from "semantic-ui-react";
import styles from "../assets/styles";

const TodoListView = ({ todoList, deleteTodo, todo }) => {
  return (
    <Grid.Column width={6} style={styles.todoList}>
      {todoList.length !== 0 ? (
        todoList
          .slice(0)
          .reverse()
          .map((todo, index) => (
            <div key={index} style={styles.todo}>
              <p>{todo.title}</p>
              <div>
                <Button
                  onClick={() => deleteTodo(todo.id)}
                  size="mini"
                  circular
                  icon="cancel"
                  style={styles.buttonDelete}
                />
              </div>
            </div>
          ))
      ) : (
        <p style={styles.listText}>Add a todo</p>
      )}
    </Grid.Column>
  );
};

export default TodoListView;
