import React from "react";
import { Grid, TextArea, Button } from "semantic-ui-react";
import styles from "../assets/styles";

const AddTodoView = ({ todo, setTodo, addTodo }) => {
  return (
    <React.Fragment>
      <Grid.Column width={2} />
      <Grid.Column width={5} textAlign="center" style={styles.addTodo}>
        <TextArea
          size="mini"
          placeholder="Add a new todo..."
          onChange={e => setTodo({ title: e.target.value })}
          value={todo.title}
          style={styles.textArea}
        />
        <Button
          circular
          icon="add"
          style={styles.buttonAdd}
          disabled={todo.title === ""}
          onClick={() => {
            addTodo(todo);
            setTodo({ title: "" });
          }}
        />
      </Grid.Column>
    </React.Fragment>
  );
};

export default AddTodoView;
